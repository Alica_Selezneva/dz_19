@extends('layout')

@section('content')

    <h1>More information</h1>

    <table border="1">
        <tr>
            <th>Customer name</th>
            <th>feedback</th>
            <th>email</th>
            <th>phone</th>
        </tr>
        <tr>
            <td>{{$orders->customer_name}}</td>
            <td>{{$orders->feedback}}</td>
            <td>{{$orders->email}}</td>
            <td>{{$orders->phone}}</td>
        </tr>
    </table>

@stop