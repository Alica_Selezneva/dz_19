@extends('layout')

@section('content')

    <h1>More information</h1>

    <table border="1">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
        </tr>
        <tr>
            <td>{{$products->title}}</td>
            <td>{{$products->description}}</td>
            <td>{{$products->price}}</td>
        </tr>
</table>

@stop