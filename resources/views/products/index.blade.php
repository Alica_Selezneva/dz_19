@extends('layout')

@section('content')
    <h1>Products</h1>
    @foreach($products as $product)
         <li><a href="/products/{{$product->id}}">{{$product->title}}</a></li>
    @endforeach

@stop

