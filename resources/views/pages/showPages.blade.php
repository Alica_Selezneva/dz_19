@extends('layout')

@section('content')

    <h1>More information</h1>

    <table border="1">
        <tr>
            <th>title</th>
            <th>alias</th>
            <th>content</th>
        </tr>
        <tr>
            <td>{{$pages->title}}</td>
            <td>{{$pages->alias}}</td>
            <td>{{$pages->content}}</td>
        </tr>
</table>

@stop