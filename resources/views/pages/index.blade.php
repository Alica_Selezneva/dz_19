@extends('layout')

@section('content')
    <h1>Products</h1>
    @foreach($pages as $page)
         <li><a href="/pages/{{$page->id}}">{{$page->title}}</a></li>
    @endforeach

@stop

