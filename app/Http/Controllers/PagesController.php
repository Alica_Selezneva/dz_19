<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages;
class PagesController extends Controller
{
    public function index()
    {
        $pages=Pages::all();
        return view('pages.index',compact('pages'));
    }

    public function showPages($id)
    {
      $pages=Pages::find($id);
        return view('pages.showPages')->with(compact('pages'));
    }
}
