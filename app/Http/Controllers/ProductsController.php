<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
class ProductsController extends Controller
{
    public function index()
    {
        $products=DB::table('products')->get();
        return view('products.index',compact('products'));
    }

    public function showProduct($id)
    {
       $products=Products::find($id);
        return view('products.showProduct')->with(compact('products'));
    }
}
