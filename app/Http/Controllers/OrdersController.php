<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
class OrdersController extends Controller
{
    public function index()
    {
        $orders=Orders::all();
        return view('orders.index',compact('orders'));
    }

    public function showOrders($id)
    {
       $orders=Orders::find($id);
        return view('orders.showOrders')->with(compact('orders'));
    }
}
