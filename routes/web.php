<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('products','ProductsController@index');
Route::get('products/{id}','ProductsController@showProduct');

Route::get('orders','OrdersController@index');
Route::get('orders/{id}','OrdersController@showOrders');

Route::get('pages','PagesController@index');
Route::get('pages/{id}','PagesController@showPages');